# OLANSI PURIFIER #

Guangzhou Olansi Air Purifier Manufactuer is a professional chinese air purifier air cleaner air filter factory with true hepa air purifier,negative ion home air purifier,mini car air purifier,Active carbon and photocatalyst filter nano air purifier,Ionizer air purifier,Room Air Purifier and so on. For PM2.5 removal,pollen and dust removal,Cigarette smoke removal，Formaldehyde and benzene removal,Bacterial removal and so on.

1.Olansi have strong R&D with 30 engineers
2.Olansi have 11 laboratories of evaluation centre
3.Media production management team and with about 20 QA&QC people
4.One-stop service, Design→Moulding→Injection→Assembly→Export
5.Olansi have our own injection factory with LG ABS for injection
6.Olansi have our own air filter and water filter factory to control the quality
7.Olansi have our own mould factory to do ODM with our customers
8.Dust-free production workshop for air purifier,water purifier and hydrogen water maker.
9.As Vendor for 10 of the top 20 MLM companies.

# About Us #

"[Olansi](https://www.olansi.net)" Established in 2009,located in Guangzhou,Olansi Healthcare Co., Ltd is a professional air purifier,water purifier,beauty instrument and hydrogen water machine manufacturer from china.
Guangzhou Olansi Healthcare Co. Ltd spends 10% of its annual sale turnover into R&D. We have a 30-engineer R&D team, thus we are able to launch five new models every year.

Specialized in Air Purifiers,Water Purifiers and Hydrogen Water Makers
For the past years, Olansi has developed an expertise in the production of air purifiers,water purifiers,from unrivaled user-friendliness and innovative design, through flawless engineering to the most stylish and appealing packaging. This marked an important chapter in Olansi history.